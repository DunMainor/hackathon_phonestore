import React, { Component } from 'react'
import {Image} from 'react-native'
import { Container,Content, Body, Button, Title, Header, Left,  Right, StyleProvider, Badge, Text, Drawer,Item, Input, View } from 'native-base'
import {Icon} from 'react-native-elements'
import {NavigationActions, StackActions} from 'react-navigation'

export default class TitleHeader extends Component {
  constructor(props) {
    super(props)
    this.state={
        showAlert:false,
        off:true,
        user:'',
        search_bar_visible:false,
    }
    this.openDrawer = this.openDrawer.bind(this)
    this.closeDrawer = this.closeDrawer.bind(this)
  }


  closeDrawer() {
    this.props.navigation.closeDrawer()
  }

  openDrawer() {
    this.props.navigation.openDrawer()
  }

  _goBack(){
    NavigationActions.back()
  }
  render(){
    let backButton =  <Left style={{paddingTop:5}} ><Button onPress={()=> {this.props.navigation.goBack()}} style={{width:50,height:50}} transparent  ><Icon name='angle-left' type='font-awesome' size={35} color='#daa521' style={{paddingTop:20}} /></Button></Left>
    const {width,height} = this.props.screenProps   
    let menuButton = <Left style={{alignSelf:'flex-start',paddingTop:10}} ><Button transparent onPress={() => this.openDrawer()} ><Icon name='bars' size={30} type='font-awesome' color='#daa521' style={{paddingTop:20}} /></Button></Left>
    
    return(   
        <StyleProvider style={this.props.screenProps.theme} >
            <Container>
                <Header style={{maxHeight:height*.1,minHeight:height*.1,}}>
                    {this.props.showMenu ? menuButton : backButton }
                    <Body style={{alignItems:'center',paddingTop:20,marginLeft:width*-.2}}>
                        <Left style={{marginRight:width*.3}} >
                            <Title style={{color:'#daa521',fontFamily:'serif',fontSize:20}} > 
                                {this.props.title}
                            </Title>
                        </Left>
                    </Body>
                </Header>
                {/* <Content>                 */}
                    {this.props.children} 
                {/* </Content> */}
            </Container>
        </StyleProvider>         
     
    )
  }
}
