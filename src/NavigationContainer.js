import React, { Component } from 'react'
import {createDrawerNavigator,createStackNavigator,NavigationActions, StackActions, createAppContainer} from 'react-navigation'
import {fromLeft,fromTop,fadeIn,zoomIn,zoomOut,flipY,flipX} from 'react-navigation-transitions'

import DrawerContent from './components/DrawerContent'
import SplashScreen from './scenes/SplashScreen'
import ViewPhone from './scenes/drawer_scenes/ViewPhone';
import StoreScene from './scenes/drawer_scenes/StoreScene';


const Drawer = createDrawerNavigator({
    Home: {screen: StoreScene},
    View: {screen: ViewPhone}
  },{
    initialRouteName: 'Home',
    headerMode : 'none',
    transitionConfig: ()=>(Math.random <= .5)? fromLeft(500) : fromTop(500),
    contentComponent: (props)=>(<DrawerContent {...props} />)
  }
)

const RootStack = createStackNavigator({
    Splash: {screen: SplashScreen},
    Drawer: {screen: Drawer}
  },{
    initialRouteName: 'Splash',
    headerMode : 'none',
    transitionConfig: ()=>(Math.random <= .5)? fromLeft(500) : fromTop(500),    
    }
)

const App = createAppContainer(RootStack)

export default App