module.exports = class AssetStore{
    constructor(){
        this.AppLogo = require('../assets/img/app_logo.png')
        this.SplashBg = require('../assets/img/splash_bg.png')
        this.DrawerTop = require('../assets/img/drawer_top.jpg')
        
    }

    AppLogo(){        return this.AppLogo}
    SplashBg(){return this.SplashBg}
    DrawerTop(){return this.DrawerTop}
}