import React,{Component} from 'react';
import {ImageBackground, Dimensions,ToastAndroid, AsyncStorage, Image,Text } from 'react-native';
import {StyleProvider, Container, Content,Button, View,  Separator} from 'native-base'
import {Icon} from 'react-native-elements'
import {StackActions,NavigationActions} from 'react-navigation'


const {width, height} = Dimensions.get('window');
export default class DrawerContent extends Component {
    constructor(props){
        super(props)
        this.state = {
            homeBg:'',
            selectBg:'',
            accountBg:'',
            earningBg:'',
            scheduleBg:'',
            pendingBg:'',
            tintItem:'',
            user_name:'John Doe'
        }
    }

    _handleClick(item){
        this.props.navigation.closeDrawer()
        this.props.navigation.navigate(item.toString()) 
    }
    _logout(){
        AsyncStorage.getAllKeys((err,keys)=>{
            if(!err){
                let counter = 0
                keys.forEach((key)=>{
                    counter++
                    AsyncStorage.removeItem(key.toString(),(err)=>{
                        if(err){
                            console.log('Error removing this key from AsyncStore :'+key.toString())
                        }
                        if(counter == keys.length ){
                            this.props.navigation.dispatch(StackActions.reset({
                                index: 0,
                                key: null,
                                actions: [NavigationActions.navigate({ routeName: 'Login' })]
                              }))
                        }
                    })
                })
            }
        })
    }

    componentDidMount = () => {
      let user_name = AsyncStorage.getItem('full_name',(err)=>{
          if(err){
              console.log('Error from the drawer retrieving username, ERROR Message: '+err.toString())
              this.setState({user_name:'New User'})
          }
      })
      if(user_name !== null){
          this.setState({user_name:user_name})
      }
    }

    _titleCase(text) {
        var firstLtr = 0;
        for (var i = 0;i < text.length;i++){
            if (i == 0 &&/[a-zA-Z]/.test(text.charAt(i))) firstLtr = 2;
            if (firstLtr == 0 &&/[a-zA-Z]/.test(text.charAt(i))) firstLtr = 2;
            if (firstLtr == 1 &&/[^a-zA-Z]/.test(text.charAt(i))){
                if (text.charAt(i) == "'"){
                    if (i + 2 == text.length &&/[a-zA-Z]/.test(text.charAt(i + 1))) firstLtr = 3;
                    else if (i + 2 < text.length &&/[^a-zA-Z]/.test(text.charAt(i + 2))) firstLtr = 3;
                }
            if (firstLtr == 3) firstLtr = 1;
            else firstLtr = 0;
            }
            if (firstLtr == 2){
                firstLtr = 1;
                text = text.substr(0, i) + text.charAt(i).toUpperCase() + text.substr(i + 1);
            }
            else {
                text = text.substr(0, i) + text.charAt(i).toLowerCase() + text.substr(i + 1);
            }
        }
            return text;
    }
    
    
    render() {
        const {assets} = this.props.screenProps.stores
        const {theme} = this.props.screenProps
        const user_name = 'John Doe'
        const avatar_uri = 'https://api.adorable.io/avatars/100/'+user_name+'.png'
        return (
            <StyleProvider style={theme}>          
                <Container >
                    <Content scrollEnabled={false} style={{maxHeight:height}} >
                        <ImageBackground source={assets.DrawerTop}
                            style={{maxHeight:height*.25,minHeight:height*.25, width:width*.8,marginBottom: 5,paddingTop:height*.12,flexDirection:'row'}} resizeMode={'stretch'} >
                            <Image source={{uri:avatar_uri}} resizeMode={'contain'} style={{width:80,height:80,flex:1,borderRadius:40,}} />
                            <Button style={{flexDirection:'column',paddingTop:height*.04,height:height*.12,flex:3,marginLeft:-20}} transparent block onPress={()=>{
                                // this._handleClick('Account')
                                ToastAndroid.showWithGravity(
                                    'Component Wiring Pending',
                                    ToastAndroid.SHORT,
                                    ToastAndroid.CENTER
                                )
                            }} >
                                <Text style={{color:'#f9f9f9',fontSize:20,alignSelf:'center',fontFamily:'serif',marginTop:20}} >{this._titleCase('John Dow')}</Text>
                                {/* <Text style={{fontSize:12,color:'#f9f9f9',marginLeft:-30}} >View Profile ></Text> */}
                            </Button>
                        </ImageBackground>
                        <View style={{minHeight:height*.05, maxHeight:height*.08, backgroundColor:(this.state.tintItem == 'Home')? 'rgba(255,255,150,100)' : '#fff'}} >
                            <Button transparent iconLeft block style={{paddingLeft:10,height:height*.1,paddingTop:15,justifyContent:'flex-start',alignItems: 'flex-start',}} onPress={()=>{
                                this._handleClick('Home')       
                            }} > 
                                <Icon name={'home'} type='font-awesome' size={25} color='rgba(0,0,0,.7)' />
                                <Text style={{fontSize:17, color:'rgba(0,0,0,.7)',flex:9,marginLeft:20}} >Store</Text>
                            </Button>
                        </View>
                        <View style={{minHeight:height*.05, maxHeight:height*.08, backgroundColor:(this.state.tintItem == 'Trips')? 'rgba(255,255,150,100)' : '#fff'}} >
                            <Button transparent iconLeft block style={{paddingLeft:10,height:height*.1,paddingTop:15,justifyContent:'flex-start',alignItems: 'flex-start',}} onPress={()=>{
                                // this._handleClick('Deliveries') 
                                ToastAndroid.showWithGravity(
                                    'Component Wiring Pending',
                                    ToastAndroid.SHORT,
                                    ToastAndroid.CENTER
                                )
                                this.props.navigation.closeDrawer()
                            }} > 
                                <Icon name={'shopping-cart'} type='font-awesome' size={25} color='rgba(0,0,0,.7)' />
                                <Text style={{fontSize:17, color:'rgba(0,0,0,.7)',flex:9,marginLeft:20}} >Cart</Text>
                            </Button>
                        </View>
                        <View style={{minHeight:height*.05, maxHeight:height*.08, backgroundColor:(this.state.tintItem == 'Ongoing')? 'rgba(255,255,150,100)' : '#fff'}} >
                            <Button transparent iconLeft block style={{paddingLeft:10,height:height*.1,paddingTop:15,justifyContent:'flex-start',alignItems: 'flex-start',}} onPress={()=>{
                                // this._handleClick('Ongoing') 
                                ToastAndroid.showWithGravity(
                                    'Component Wiring Pending',
                                    ToastAndroid.SHORT,
                                    ToastAndroid.CENTER
                                )
                                this.props.navigation.closeDrawer()
                            }} > 
                                <Icon name={'money'} type='font-awesome' size={25} color='rgba(0,0,0,.7)' />
                                <Text style={{fontSize:17, color:'rgba(0,0,0,.7)',flex:9,marginLeft:20}} >Offers</Text>
                            </Button>
                        </View>

                        <View style={{minHeight:height*.05, maxHeight:height*.08, backgroundColor:(this.state.tintItem == 'Ongoing')? 'rgba(255,255,150,100)' : '#fff'}} >
                            <Button transparent iconLeft block style={{paddingLeft:10,height:height*.1,paddingTop:15,justifyContent:'flex-start',alignItems: 'flex-start',}} onPress={()=>{
                                // this._handleClick('SelAccount') 
                                ToastAndroid.showWithGravity(
                                    'Component Wiring Pending',
                                    ToastAndroid.SHORT,
                                    ToastAndroid.CENTER
                                )
                                this.props.navigation.closeDrawer()
                            }} > 
                                <Icon name={'cubes'} type='font-awesome' size={25} color='rgba(0,0,0,.7)' />
                                <Text style={{fontSize:17, color:'rgba(0,0,0,.7)',flex:9,marginLeft:20}} >Account</Text>
                            </Button>
                        </View>
                        
                        <View style={{marginTop:height*.15}}>
                        <View style={{height:2,borderBottomColor:'gray',borderBottomWidth:1,marginBottom:5,marginTop:30}}  ></View>

                            
                            <View style={{minHeight:height*.05, maxHeight:height*.08, backgroundColor:(this.state.tintItem == 'About')? 'rgba(255,255,150,100)' : '#fff'}} >
                                <Button transparent block iconLeft style={{paddingLeft:10,height:height*.1,paddingTop:15,justifyContent:'flex-start',alignItems: 'flex-start',}} onPress={()=>{
                                    // this._logout()
                                    ToastAndroid.showWithGravity(
                                        'Component Wiring Pending',
                                        ToastAndroid.SHORT,
                                        ToastAndroid.CENTER
                                    )
                                    this.props.navigation.closeDrawer()
                                }} > 
                                    <Icon name={'sign-out'} type='font-awesome' size={25} color='rgba(0,0,0,.7)' />
                                    <Text style={{fontSize:17, color:'rgba(0,0,0,.7)',flex:9,marginLeft:20}} >Sign In</Text>
                                </Button>
                            </View>
                        </View>
                    </Content>
                </Container>
                </StyleProvider>
        );
    }
}

    